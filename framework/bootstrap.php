<?php

use PhrameWurx\Application;
use PhrameWurx\Http\ResponseDispatcher;
use Phroute\Phroute\RouteCollector;
use PhrameWurx\Http\RouteLoader;
use Dotenv\Dotenv;

/**
 * Load up env/config
 */
$dotenv = Dotenv::createImmutable(BASE_DIR);
$dotenv->load();



/**
 * Initialize Application and Container
 */
$app = Application::instance();

/**
 * Initialize router and dispatcher
 */
$app->getContainer()->add('app.router', new RouteCollector());
$app->getContainer()->add('app.http.dispatcher', function() use($app) {
    $router = $app->getContainer()->get('app.router');
    return new ResponseDispatcher($router);
});

/** Load API Routes */
RouteLoader::loadRoutesFile();

