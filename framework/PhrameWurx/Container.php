<?php


namespace PhrameWurx;


use Prophecy\Exception\Doubler\ClassNotFoundException;
use SplObjectStorage;

class Container
{
    /**
     * @var array
     */
    protected $storage;

    public function __construct()
    {
        $this->storage = [];
    }

    /**
     * @param $key
     * @param callable $createInstance
     * @return $this
     */
    public function singleton($key, Callable $createInstance) {
        $this->storage[$key] = $createInstance;
        return $this;
    }

    /**
     * add a item to the container
     * @param $key
     * @param $value
     * @return $this
     */
    public function add($key, $value) {
        $this->storage[$key] = $value;
        return $this;
    }

    /**
     * Retrieve an object from the container
     * @param $key
     * @return mixed
     * @throws ClassNotFoundException
     */
    public function get($key) {
        if(isset($this->storage[$key])) {
            $item =  $this->storage[$key];
            if(is_callable($item)) {
                return call_user_func($item);
            }
            return $item;
        }
        throw new ClassNotFoundException("Could not find an object keyed to: {$key}");
    }

}