<?php


namespace PhrameWurx;

/**
 * Class Application
 * @package PhrameWurx
 */
class Application
{

    /**
     * @var Application
     */
    protected static $instance = null;

    /**
     * @var Container
     */
    protected $container;


    /**
     * Singleton getter
     * @return Application
     */
    public static function instance() {
        if(null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Private constructor for singleton
     */
    private function __constructor() {
        $this->container = new Container();
    }

    /**
     * @return Container
     */
    public function getContainer() : Container
    {
        return $this->container;
    }

    /**
     * @param Container $container
     * @return Application
     */
    public function setContainer($container) : Application
    {
        $this->container = $container;
        return $this;
    }


}