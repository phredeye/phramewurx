<?php


namespace PhrameWurx\Http;


use Phroute\Phroute\Dispatcher;
use Phroute\Phroute\RouteCollector;

/**
 * Class ResponseDispatcher
 * A Decorator class for Phroute/Dispatcher
 *
 * @package PhrameWurx\Http
 *
 * You can cache the return value from $router->getData()
 * so you don't have to create the routes each request - massive speed gains
 */
class ResponseDispatcher
{
    /**
     * @var Dispatcher
     */
    protected $dispatcher;

    /**
     * @var
     */
    protected $router;


    /**
     * ResponseDispatcher constructor.
     * @param RouteCollector $router
     */
    public function __construct(RouteCollector $router)
    {
        $this->router = $router;
    }

    /**
     * @return Dispatcher
     */
    public function getDispatcher(): Dispatcher
    {
        return $this->dispatcher;
    }

    /**
     * @param Dispatcher $dispatcher
     * @return ResponseDispatcher
     */
    public function setDispatcher(Dispatcher $dispatcher): ResponseDispatcher
    {
        $this->dispatcher = $dispatcher;
        return $this;
    }

    /**
     * Server Response
     * @return mixed|null
     */
    public function dispatch()
    {
        // get HTTP METHOD (GET, POST, PUT, PATCH, DELETE, HEAD)
        $requestMethod = $_SERVER['REQUEST_METHOD'];
        // HTTP REQUEST URI e.g. /api/users/43
        $requestURI = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        // initialize dispatcher with all routes registered up to this point
        $this->dispatcher = new Dispatcher($this->router->getData());

        // if there is a match in the collector to the method and uri
        $response = $this->dispatcher->dispatch($requestMethod, $requestURI);

        // return the http response
        return $response;
    }
}