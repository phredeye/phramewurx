<?php


namespace PhrameWurx\Http;


use PhrameWurx\Application;

class RouteLoader
{
    /**
     * @return mixed
     */
    public static function loadRoutesFile()
    {
        $router = Application::instance()->getContainer()->get('app.router');
        return include_once(BASE_DIR . "/routes/api.php");
    }
}